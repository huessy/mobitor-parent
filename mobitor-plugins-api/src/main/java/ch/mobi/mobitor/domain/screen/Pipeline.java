package ch.mobi.mobitor.domain.screen;

/*-
 * §
 * mobitor-plugins-api
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.plugins.api.domain.screen.information.ApplicationInformation;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

public interface Pipeline {

    void resetRuleEvaluation(String type);

    void updateRuleEvaluation(String type, RuleEvaluation ruleEvaluation);

    RuleEvaluationSeverity getRuleEvaluationSeverity();

    void setLastRuleCompliantDate(Date date);

    Map<String, ServerContext> getServerContextMap();

    String getAppServerName();

    void addInformation(String environment, String applicationName, ApplicationInformation applicationInformation);

    List<ApplicationInformation> getMatchingInformation(String type, String environment, String applicationName);

    Set<String> getApplicationNames();

}
