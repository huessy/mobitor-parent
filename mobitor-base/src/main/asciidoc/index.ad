= Mobitor

The 'Mobitor' is the Mobiliar Build Monitor (hence its name). In summary its simply an application that shows the status
of builds, deployments, code analysis and runtime information on a screen (in a browser). To be used as a build monitor.
The development teams in the Mobiliar use it to simplify the transparency of their developed services, metrics and
delivery pipeline status along with information on service health.

In short this can be done:

image::images/mobitor-screen.jpg[]

== Overview

A build monitor displays information on a screen. So its mainly static content and does not come with a lot of
interaction features. The Mobitor uses a json format to describe a screen. Several sources can be configured:

 * Liima (for deployment information)
 * TeamCity (for CI builds and test-runs)
 * SonarQube (for static code analysis and test coverage)
 * Jira (for issues and jira filter results)
 * BitBucket Server (for the time between commit and deployment to an environment)
 * Rest API calls (based on a swagger.json uri the Mobitor can show some health information)
 * some more that are rather Mobiliar specific (see plugins below)
 * Kubernetes Job information (Batch runs)


== Requirements

 * Java 11 (OpenJDK)
 * Maven 3.3.9 (or newer)

The Mobitor can be deployed as a docker image. It does not require a storage backend, all data is held in memory. It
uses https://spring.io/projects/spring-boot[Spring Boot 2.x], Spring MVC and https://www.thymeleaf.org/[Thymleaf] to
render the web pages.


== Features

The single sources of information are put into plugins:

 * link:plugins/bitbucket/index.html[BitBucket Plugin]
 * link:plugins/edwh/index.html[Edwh Plugin]
 * link:plugins/dwh/index.html[Dwh Plugin]
 * link:plugins/jira/index.html[Jira Plugin]
 * link:plugins/kubernetes/index.html[Kubernetes Plugin]
 * link:plugins/liima/index.html[Liima Plugin]
 * link:plugins/nexusiq/index.html[Nexus IQ Plugin]
 * link:plugins/rest/index.html[Rest / Swagger.json Plugin]
 * link:plugins/sonarqube/index.html[Sonarqube Plugin]
 * link:plugins/streamworks/index.html[Streamworks Plugin]
 * link:plugins/swd/index.html[SWD Plugin]
 * link:plugins/teamcity/index.html[TeamCity Plugin]
 * link:plugins/zaproxy/index.html[ZA Proxy Plugin]

Some additional features include:

 * link:jira-changelog.html[Changelog in Jira]: creating labels in Jira when a commit in BitBucket was deployed to an environment
 * allow link:screen-generation.html[generation of Screen configurations] (in addition to manually configure `screen.json` files)
 * implement link:mobitor-rules.html[custom rules] to guide changes and improvements

== Configuration

=== For screen users

 * link:create-a-new-screen.html[Create a new Screen]

=== For Mobitor Operations / Maintenance

 * link:getting-started.html[get started] with your own Mobitor instance
 * link:plugins/rest/index.html[Configure your network zones] (required for the REST plugin and the changelog module)
 * link:monitoring-with-prometheus.html[Monitoring Mobitor] with Prometheus


== Gallery

So after all the work, what will you get?! link:screen-gallery.html[Head over to our gallery] of screens that our
development teams at the Mobiliar use.


== Contributions

=== Issues

If you found a bug, or you are missing a feature or have a suggestion please report it in the issue tracker.

=== Plugin Contributions

If you created your own plugin and you want to make it available in the default Mobitor distribution, please also create
an issue in the issue tracker for this so people know you are working on this. Try to keep a similar structure like the
other plugins and simply create a pull-request!



_Mobitor Parent Version:_ {project-version}
