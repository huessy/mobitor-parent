package ch.mobi.mobitor.plugin.test.domain;

/*-
 * §
 * mobitor-plugins-test
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.domain.screen.RuleEvaluationSeverity;
import ch.mobi.mobitor.domain.screen.Pipeline;
import ch.mobi.mobitor.domain.screen.RuleEvaluation;
import ch.mobi.mobitor.domain.screen.ServerContext;
import ch.mobi.mobitor.plugins.api.domain.screen.information.ApplicationInformation;

import java.util.Date;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * This class serves as fake implementation to be available to plugins that implement tests that use a Pipeline instance
 */
public class TestPipeline implements Pipeline {

    private final String serverName;
    /** environment is used as key */
    private final Map<String, ServerContext> serverContextMap;
    private Set<String> applicationNames = new LinkedHashSet<>();

    public TestPipeline(String serverName, Map<String, ServerContext> serverContextMap) {
        this.serverName = serverName;
        this.serverContextMap = serverContextMap;
    }

    @Override
    public void resetRuleEvaluation(String type) {

    }

    @Override
    public void updateRuleEvaluation(String type, RuleEvaluation ruleEvaluation) {

    }

    @Override
    public RuleEvaluationSeverity getRuleEvaluationSeverity() {
        return RuleEvaluationSeverity.CONTAINS_ERRORS;
    }

    @Override
    public void setLastRuleCompliantDate(Date date) {

    }

    @Override
    public Map<String, ServerContext> getServerContextMap() {
        return serverContextMap;
    }

    @Override
    public String getAppServerName() {
        return serverName;
    }

    @Override
    public void addInformation(String environment, String applicationName, ApplicationInformation applicationInformation) {
        ServerContext serverContext = this.serverContextMap.get(environment);
        if (serverContext==null){
            throw new IllegalStateException("Environment ["+environment+"] is not declared in the environments list");
        }
        serverContext.addInformation(applicationName, applicationInformation);

        applicationNames.add(applicationName);
    }

    @Override
    public List<ApplicationInformation> getMatchingInformation(String type, String environment, String applicationName) {
        ServerContext serverContext = this.serverContextMap.get(environment);
        List<ApplicationInformation> informationList = serverContext.getMatchingInformation(type, applicationName);
        return informationList;
    }

    @Override
    public Set<String> getApplicationNames() {
        return applicationNames;
    }
}
