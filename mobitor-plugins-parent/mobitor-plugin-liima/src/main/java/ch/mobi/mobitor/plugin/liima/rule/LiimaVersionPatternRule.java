package ch.mobi.mobitor.plugin.liima.rule;

/*-
 * §
 * mobitor-plugin-liima
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.domain.screen.PipelineRule;
import ch.mobi.mobitor.domain.screen.Pipeline;
import ch.mobi.mobitor.domain.screen.RuleEvaluation;
import ch.mobi.mobitor.domain.screen.RuleViolationSeverity;
import ch.mobi.mobitor.domain.screen.ServerContext;
import ch.mobi.mobitor.domain.screen.information.VersionInformation;
import ch.mobi.mobitor.plugin.liima.service.ServersConfigurationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static ch.mobi.mobitor.plugin.liima.domain.LiimaInformation.LIIMA;
import static org.apache.commons.lang3.StringUtils.isBlank;
import static org.apache.commons.lang3.StringUtils.isNotBlank;


public class LiimaVersionPatternRule implements PipelineRule {

    private final String VERSION_REGEX = "^(?:(0\\.|([1-9]+\\d*)\\.))+(?:(0\\.|([1-9]+\\d*)\\.))+(0|([1-9]+\\d*))$";
    private final Pattern VERSION_PATTERN = Pattern.compile(VERSION_REGEX);

    private final ServersConfigurationService serversConfigurationService;

    @Autowired
    public LiimaVersionPatternRule(ServersConfigurationService serversConfigurationService) {
        this.serversConfigurationService = serversConfigurationService;
    }

    @Override
    public void evaluateRule(Pipeline pipeline, RuleEvaluation re) {
        String serverName = pipeline.getAppServerName();
        Map<String, ServerContext> serverContextMap = pipeline.getServerContextMap();
        for (Map.Entry<String, ServerContext> entry : serverContextMap.entrySet()) {
            String env = entry.getKey();
            ServerContext serverContext = entry.getValue();

            serverContext.getMatchingInformation(LIIMA).stream()
                         .filter(info -> info instanceof VersionInformation)
                         .filter(info -> isNotBlank(((VersionInformation) info).getVersion()))
                         .filter(info -> ruleViolated(((VersionInformation) info).getVersion()))
                         .forEach(info -> re.addViolation(env, info, RuleViolationSeverity.ERROR, "version format is invalid: " + ((VersionInformation) info).getVersion()))
            ;
        }
    }

    boolean ruleViolated(String version) {
        if (isBlank(version)) {
            return true;
        }

        final Matcher matcher = VERSION_PATTERN.matcher(version);

        boolean matches = matcher.matches();
        return !matches;
    }

    @Override
    public boolean validatesType(String type) {
        return LIIMA.equals(type);
    }
}
