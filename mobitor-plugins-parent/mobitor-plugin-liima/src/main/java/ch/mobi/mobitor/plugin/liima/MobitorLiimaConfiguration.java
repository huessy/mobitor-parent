package ch.mobi.mobitor.plugin.liima;

/*-
 * §
 * mobitor-plugin-liima
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "mobitor.plugins.liima")
public class MobitorLiimaConfiguration {

    private String restBaseUri;
    private String webUrl;
    private String angularUrl;

    public String getRestBaseUri() {
        return restBaseUri;
    }

    public void setRestBaseUri(String restBaseUri) {
        this.restBaseUri = restBaseUri;
    }

    public String getWebUrl() {
        return webUrl;
    }

    public void setWebUrl(String webUrl) {
        this.webUrl = webUrl;
    }

    public String getAngularUrl() {
        return angularUrl;
    }

    public void setAngularUrl(String angularUrl) {
        this.angularUrl = angularUrl;
    }
}
