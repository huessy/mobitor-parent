package ch.mobi.mobitor.plugin.teamcity.service;

/*-
 * §
 * mobitor-plugin-teamcity
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.metrics.cache.CacheMetricsRegistrar;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.List;

import static ch.mobi.mobitor.plugin.teamcity.service.scheduling.TeamCityBuildStatusCollector.CACHE_NAME_TEAMCITY_BUILDS;
import static ch.mobi.mobitor.plugin.teamcity.service.scheduling.TeamCityBuildStatusCollector.CACHE_NAME_TEAMCITY_BUILD_STATISTICS;
import static ch.mobi.mobitor.plugin.teamcity.service.scheduling.TeamCityBuildStatusCollector.CACHE_NAME_TEAMCITY_PROJECTS;
import static java.util.Arrays.asList;

@Component
public class TeamCityPluginCacheMetricsRegisterer {

    private final CacheManager cacheManager;
    private final CacheMetricsRegistrar cacheMetricsRegistrar;

    @Autowired
    public TeamCityPluginCacheMetricsRegisterer(CacheManager cacheManager, CacheMetricsRegistrar cacheMetricsRegistrar) {
        this.cacheManager = cacheManager;
        this.cacheMetricsRegistrar = cacheMetricsRegistrar;
    }

    @PostConstruct
    public void registerCachesAtMetrics() {
        List<String> cacheNames = asList(
                CACHE_NAME_TEAMCITY_BUILDS,
                CACHE_NAME_TEAMCITY_BUILD_STATISTICS,
                CACHE_NAME_TEAMCITY_PROJECTS
        );
        cacheNames.forEach(cn -> {
            Cache cache = cacheManager.getCache(cn);
            cacheMetricsRegistrar.bindCacheToRegistry(cache);
        });
    }
}
