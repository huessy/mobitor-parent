package ch.mobi.mobitor.plugin.streamworks.service.client;

/*-
 * §
 * mobitor-plugin-streamworks
 * --
 * Copyright (C) 2019 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.plugin.streamworks.service.client.dto.StreamworksJob;
import ch.mobi.mobitor.plugins.api.domain.screen.information.ApplicationInformation;
import static ch.mobi.mobitor.plugin.streamworks.domain.StreamworksLastRunStatusInformation.STREAMWORKS_LAST_RUN_STATUS;


import java.util.List;
import java.util.Objects;

public class StreamworksLastRunStatus {

    private String verarbeitung;
    private String status;
    private List<StreamworksJob> jobs;

    public StreamworksLastRunStatus() {
    }

    public StreamworksLastRunStatus(String verarbeitung, String status, List<StreamworksJob> jobs) {
        this.verarbeitung = verarbeitung;
        this.status = status;
        this.jobs = jobs;
    }

    public String getStatus() {
        return status;
    }

    @Override
    public boolean equals(Object o){
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        StreamworksLastRunStatus that = (StreamworksLastRunStatus) o;
        boolean jobsAreEqualNull = (jobs == null && that.jobs == null);
        return  (jobsAreEqualNull ||
                jobs.equals(that.jobs)
                   ) &&
                status.equals(that.status) &&
                verarbeitung.equals(that.verarbeitung);
    }

    @Override
    public String toString() {
        String jobsString = "";
        if (jobs != null){
            jobsString = jobs.toString();
        }
        return "StreamworksLastRunStatus{" +
                "verarbeitung='" + verarbeitung + '\'' +
                ", status='" + status + '\'' +
                ", jobs=" + jobsString +
                '}';
    }

    @Override
    public int hashCode() {
        int jobHashcode = 0;
        if (jobs != null){
            jobHashcode = jobs.hashCode();
        }
        return Objects.hash(verarbeitung, status, jobHashcode);
    }

    public String getVerarbeitung() {
        return verarbeitung;
    }

    public void setVerarbeitung(String verarbeitung) {
        this.verarbeitung = verarbeitung;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<StreamworksJob> getJobs() {
        return jobs;
    }

    public void setJobs(List<StreamworksJob> jobs) {
        this.jobs = jobs;
    }

}
