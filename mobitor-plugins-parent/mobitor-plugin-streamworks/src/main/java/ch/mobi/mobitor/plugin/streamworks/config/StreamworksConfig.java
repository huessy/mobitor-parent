package ch.mobi.mobitor.plugin.streamworks.config;

/*-
 * §
 * mobitor-plugin-streamworks
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.plugins.api.PluginConfig;

import java.util.Map;

public class StreamworksConfig extends PluginConfig {

    /** Label which will be shown in directly in the Mobitor */
    private final String label = "Stream";

    private final String process;

    public StreamworksConfig(String serverName, Map<String, String> server, String applicationName, String environment) {
        if (serverName != null) {
            if (server != null) {
                throw illegalConfig();
            }
            setServerName(serverName);
            this.process = serverName;
        } else if (server != null) {
            if (server.size() != 1) {
                throw illegalConfig();
            }
            Map.Entry<String, String> entry = server.entrySet().iterator().next();
            setServerName(entry.getKey());
            this.process = entry.getValue();
        } else {
            throw illegalConfig();
        }
        setApplicationName(applicationName != null ? applicationName : "STREAMWORKS");
        setEnvironment(environment);
    }

    private RuntimeException illegalConfig() {
        return new IllegalArgumentException("Either serverName or server with exactly one map entry must be given.");
    }


    public String getProcess() {
        return process;
    }

    public String getLabel() {
        return label;
    }

}
