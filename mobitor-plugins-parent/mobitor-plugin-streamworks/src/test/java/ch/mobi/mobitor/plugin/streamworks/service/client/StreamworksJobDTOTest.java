package ch.mobi.mobitor.plugin.streamworks.service.client;

/*-
 * §
 * mobitor-plugin-streamworks
 * --
 * Copyright (C) 2019 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.plugin.streamworks.service.client.dto.StreamworksJob;
import ch.mobi.mobitor.plugin.streamworks.service.client.dto.StreamworksStream;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.json.JSONArray;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

public class StreamworksJobDTOTest {

    private final StreamworksJob streamworksJob = new StreamworksJob(
            "pdv_V_partnerDetektionJob",
            1L,
            "0",
            "Completed",
            "2017-06-27 12:37:43",
            "2017-06-27 12:37:57",
            "DIVE_V_PARHKx-S020"
    );

    private StreamworksLastRunStatus pdvHousekeepingStatus = new StreamworksLastRunStatus(
            "pdv-partner-housekeeping-javabatch",
            "Running",
            new ArrayList<>(List.of(streamworksJob))

    );

    private final StreamworksJob streamworksJob2 = new StreamworksJob(
            "pdv_V_partnerDetektionJob",
            2L,
            "0",
            "Completed",
            "2017-06-27 12:37:43",
            "2017-06-27 12:37:57",
            "DIVE_V_PARHKx-S020"
    );

    private StreamworksLastRunStatus pdvHousekeepingStatus2 = new StreamworksLastRunStatus(
            "pdv-partner-housekeeping-javabatch",
            "Running",
            new ArrayList<>(List.of(streamworksJob2))

    );

    private StreamworksLastRunStatus pdvHousekeepingStatus3 = new StreamworksLastRunStatus(
            "pdv-partner-housekeeping-javabatch",
            "Running",
            null

    );
    private StreamworksLastRunStatus pdvHousekeepingStatus4 = new StreamworksLastRunStatus(
            "pdv-partner-housekeeping-javabatch",
            "AbnormallyEnded",
            null

    );
    private StreamworksLastRunStatus pdvHousekeepingStatus5 = new StreamworksLastRunStatus(
            "pdv-partner-housekeeping-javabatch",
            "Running",
            new ArrayList<>(List.of(streamworksJob))

    );
    private StreamworksLastRunStatus pdvHousekeepingStatus6 = new StreamworksLastRunStatus(
            "pdv-partner-housekeeping-javabatch",
            "Running",
            null

    );

    private StreamworksStream dabStreamworksStream = new StreamworksStream(
            "SERVER1_T_012345-T001",
            1L,
            "Completed",
            "2020-01-17 14:15:15",
            "2020-01-17 14:17:51",
            "https://stweb.foo.bar.ch/dummy1/server1_t_012345-t001",
            "https://stweb.foo.bar.ch/dummy2/dummy3\u0026planDate=01-17-2020\u0026stream=SERVER1_T_012345-T001\u0026runNumber=1"
    );

    @Test
    public void StreamworksLastRunStatusEqualsPositiveFalse() {
        // Both with different Jobs
        assertNotEquals(pdvHousekeepingStatus, pdvHousekeepingStatus2);
        // Only one with a Job
        assertNotEquals(pdvHousekeepingStatus, pdvHousekeepingStatus3);
        // Both without a Job but different status
        assertNotEquals(pdvHousekeepingStatus3, pdvHousekeepingStatus4);
    }

    @Test
    public void StreamworksLastRunStatusEqualsPositiveTrue() {
        // Both with a Job
        assertEquals(pdvHousekeepingStatus, pdvHousekeepingStatus5);
        // Both without a Job
        assertEquals(pdvHousekeepingStatus3, pdvHousekeepingStatus6);
    }

    @Test
    public void StreamworksStreamComparison() throws JsonProcessingException {
        String jsonStream =
                "{\n" +
                        "  \"StreamName\":  \"SERVER1_T_012345-T001\",\n" +
                        "  \"RunNumber\":  1,\n" +
                        "  \"StreamRunStatus\":  \"Completed\",\n" +
                        "  \"StreamStartDateTime\":  \"2020-01-17 14:15:15\",\n" +
                        "  \"StreamEndDateTime\":  \"2020-01-17 14:17:51\",\n" +
                        "  \"DocumentationLink\":  \"https://stweb.foo.bar.ch/dummy1/server1_t_012345-t001\",\n" +
                        "  \"RuntimeLink\":  \"https://stweb.foo.bar.ch/dummy2/dummy3\\u0026planDate=01-17-2020\\u0026stream=SERVER1_T_012345-T001\\u0026runNumber=1\"\n" +
                        "}";
        ObjectMapper mapper = new ObjectMapper();
        StreamworksStream streamworksStream = mapper.readValue(jsonStream, StreamworksStream.class);
        assertEquals(dabStreamworksStream, streamworksStream);
    }

}
