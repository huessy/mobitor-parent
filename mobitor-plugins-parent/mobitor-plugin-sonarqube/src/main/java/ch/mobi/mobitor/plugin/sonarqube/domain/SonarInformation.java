package ch.mobi.mobitor.plugin.sonarqube.domain;

/*-
 * §
 * mobitor-plugin-sonarqube
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.plugins.api.domain.screen.information.ApplicationInformation;

import static ch.mobi.mobitor.plugin.sonarqube.rule.SonarReportLimitsRule.DEFAULT_BLOCKER_LIMIT;
import static ch.mobi.mobitor.plugin.sonarqube.rule.SonarReportLimitsRule.DEFAULT_COVERAGE_LIMIT;
import static ch.mobi.mobitor.plugin.sonarqube.rule.SonarReportLimitsRule.DEFAULT_CRITICAL_LIMIT;

public class SonarInformation implements ApplicationInformation {

    public static final String SONAR = "sonar";

    private final String projectKey;
    private final boolean strictViolations;

    private Float coverageLimit;
    private Integer criticalLimit;
    private Integer blockerLimit;

    private int blockers;
    private int criticals;
    private float coverage;
    private float itCoverage;
    private float overallCoverage;
    private float lineCoverage;
    private float overallLineCoverage;
    private float duplicatedBlocks;

    public SonarInformation(String projectKey, boolean strictViolations) {
        this.projectKey = projectKey;
        this.strictViolations = strictViolations;
    }

    public String getProjectKey() {
        return projectKey;
    }

    public boolean getStrictViolations() {
        return strictViolations;
    }

    @Override
    public String getType() {
        return SONAR;
    }

    @Override
    public boolean hasInformation() {
        return true;
    }

    public void setBlockers(int blockers) {
        this.blockers = blockers;
    }

    public int getBlockers() {
        return blockers;
    }

    public void setCriticals(int criticals) {
        this.criticals = criticals;
    }

    public int getCriticals() {
        return criticals;
    }

    public void setCoverage(float coverage) {
        this.coverage = coverage;
    }

    public float getCoverage() {
        return coverage;
    }

    public void setItCoverage(float itCoverage) {
        this.itCoverage = itCoverage;
    }

    public float getItCoverage() {
        return itCoverage;
    }

    public void setOverallCoverage(float overallCoverage) {
        this.overallCoverage = overallCoverage;
    }

    public float getOverallCoverage() {
        return overallCoverage;
    }


    public void setLineCoverage(float lineCoverage) {
        this.lineCoverage = lineCoverage;
    }

    public float getLineCoverage() {
        return lineCoverage;
    }

    public float getOverallLineCoverage() {
        return overallLineCoverage;
    }

    public void setOverallLineCoverage(float overallLineCoverage) {
        this.overallLineCoverage = overallLineCoverage;
    }

    public float getMaxCoverage() {
        float cov = getCoverage();
        float overallCov = getOverallCoverage();
        float lineCov = getLineCoverage();
        float overallLineCov = getOverallLineCoverage();

        return Math.max(Math.max(cov, overallCov), Math.max(lineCov, overallLineCov));
    }

    public float getViolationsCoverage() {
        return strictViolations ? coverage : getMaxCoverage();
    }

    public float getDuplicatedBlocks() {
        return duplicatedBlocks;
    }

    public void setDuplicatedBlocks(float duplicatedBlocks) {
        this.duplicatedBlocks = duplicatedBlocks;
    }

    public Integer getBlockerLimit() {
        return blockerLimit;
    }

    public void setBlockerLimit(Integer blockerLimit) {
        this.blockerLimit = blockerLimit;
    }

    public Integer getCriticalLimit() {
        return criticalLimit;
    }

    public void setCriticalLimit(Integer criticalLimit) {
        this.criticalLimit = criticalLimit;
    }

    public Float getCoverageLimit() {
        return coverageLimit;
    }

    public void setCoverageLimit(Float coverageLimit) {
        this.coverageLimit = coverageLimit;
    }

    /**
     * Return all the limits in a String format to display.
     *
     * @return a String containing all the limits
     */
    public String getLimitsValues() {
        return String.format("> %d, > %d, < %.2f%s",
                blockerLimit == null ? DEFAULT_BLOCKER_LIMIT : blockerLimit,
                criticalLimit == null ? DEFAULT_CRITICAL_LIMIT : criticalLimit,
                coverageLimit == null ? DEFAULT_COVERAGE_LIMIT : coverageLimit, "%");
    }
}
