package ch.mobi.mobitor.plugin.rest.service.restservice.interpreters;

/*-
 * §
 * mobitor-plugin-rest
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.plugin.rest.domain.RestServiceResponse;
import ch.mobi.mobitor.plugin.rest.service.restservice.SwaggerEndpointInterpreter;
import org.apache.http.HttpResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.cache.annotation.Cacheable;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

import static ch.mobi.mobitor.plugin.rest.domain.ResponseInterpretation.FAILURE;
import static ch.mobi.mobitor.plugin.rest.domain.RestServiceResponse.createErrorRestServiceResponse;
import static ch.mobi.mobitor.plugin.rest.domain.RestServiceResponse.createSuccessRestServiceResponse;
import static ch.mobi.mobitor.plugin.rest.service.RestPluginConstants.CACHE_NAME_REST_RESPONSES;

// Disabled until upgrade to JES7 v.3.x
//@Component
public class Jes7EndpointSecuredInterpreter implements SwaggerEndpointInterpreter {

    private static final Logger LOG = LoggerFactory.getLogger(Jes7EndpointSecuredInterpreter.class);

    private final RestServiceHttpRequestExecutor restServiceHttpRequestExecutor;

    @Autowired
    public Jes7EndpointSecuredInterpreter(@Qualifier("anonymous") RestServiceHttpRequestExecutor restServiceHttpRequestExecutor) {
        this.restServiceHttpRequestExecutor = restServiceHttpRequestExecutor;
    }

    @Override
    public Predicate<String> getMatchPredicate() {
        return path -> path.contains("/jap/properties");
    }

    @Override
    @Cacheable(cacheNames = CACHE_NAME_REST_RESPONSES)
    public RestServiceResponse fetchResponse(String uri) {
        try {
            HttpResponse httpResponse = restServiceHttpRequestExecutor.execute(uri);

            int statusCode = httpResponse.getStatusLine().getStatusCode();
            // this must be an unauthorized 401 or forbidden 403 status code:
            if (statusCode == 401 || statusCode == 403) {
                RestServiceResponse restServiceResponse = createSuccessRestServiceResponse(uri, 244, -1);
                return restServiceResponse;

            } else {
                List<String> messages = new ArrayList<>();
                messages.add("Endpoint not secured!");
                return new RestServiceResponse(uri, statusCode, FAILURE, messages, -1);
            }

        } catch (Exception e) {
            LOG.error("Could not query: " + uri, e);
            return createErrorRestServiceResponse(uri, -50);
        }
    }

}
