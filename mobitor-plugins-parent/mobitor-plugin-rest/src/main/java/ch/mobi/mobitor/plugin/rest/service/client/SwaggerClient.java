package ch.mobi.mobitor.plugin.rest.service.client;

/*-
 * §
 * mobitor-plugin-rest
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import static ch.mobi.mobitor.plugin.rest.service.restservice.RequestHeaderConstants.HEADER_CALLER;
import static ch.mobi.mobitor.plugin.rest.service.restservice.RequestHeaderConstants.HEADER_CALLER_VALUE;
import static ch.mobi.mobitor.plugin.rest.service.restservice.RequestHeaderConstants.HEADER_CALLER_VERSION;
import static ch.mobi.mobitor.plugin.rest.service.restservice.RequestHeaderConstants.HEADER_CORR_ID;
import static ch.mobi.mobitor.plugin.rest.service.restservice.RequestHeaderConstants.HEADER_X_USER_ID;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.UUID;

import org.apache.http.client.fluent.Executor;
import org.apache.http.client.fluent.Request;
import org.jetbrains.annotations.TestOnly;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;

import ch.mobi.mobitor.plugin.rest.RestPluginConfiguration;
import ch.mobi.mobitor.plugin.rest.domain.swagger.SwaggerResponse;

@Component
public class SwaggerClient {

    private static final Logger LOG = LoggerFactory.getLogger(SwaggerClient.class);

    private final RestPluginConfiguration restPluginConfiguration;
    private final String username;

    @Autowired
    public SwaggerClient(RestPluginConfiguration restPluginConfiguration,
            @Value("${restService.authentification.username}") String username) {
        this.restPluginConfiguration = restPluginConfiguration;
        this.username = username;
    }

    public SwaggerResponse retrieveApiSpec(String apiSpecUri) throws IOException, URISyntaxException {
        try {
            LOG.debug("GET: " + apiSpecUri);
            String response = executeGetRequest(apiSpecUri);
            LOG.trace("API spec response as string: " + response);
            if (apiSpecUri.endsWith("yaml")) {
                LOG.trace("Deserializing API spec in YAML format. uri=" + apiSpecUri);
                return createSwaggerResponseFromYaml(response);
            } else {
                LOG.trace("Deserializing API spec in JSON format. uri=" + apiSpecUri);
                return createSwaggerResponseFromJson(response);
            }
        } catch (IOException ex) {
            LOG.error("Could not retrieve API spec from uri: " + apiSpecUri);
            LOG.error("Reason: " + ex.getMessage());
            throw ex;
        } catch (URISyntaxException ex) {
            LOG.error("Could not parse API spec uri: " + ex.getMessage());
            throw ex;
        }
    }

    @TestOnly
    String executeGetRequest(String swaggerJsonUri) throws IOException, URISyntaxException {
        return Executor.newInstance()
                .execute(Request.Get(new URI(swaggerJsonUri))
                        .addHeader("accept", "application/json")
                        .addHeader(HEADER_CALLER, HEADER_CALLER_VALUE)
                        .addHeader(HEADER_CALLER_VERSION, restPluginConfiguration.getVersion())
                        .addHeader(HEADER_CORR_ID, String.valueOf(UUID.randomUUID()))
                        .addHeader(HEADER_X_USER_ID, username)
                        .connectTimeout(3000)
                        .socketTimeout(3000))
                .returnContent()
                .asString();
    }

    private SwaggerResponse createSwaggerResponseFromJson(String jsonContent) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
        return mapper.readValue(jsonContent, SwaggerResponse.class);
    }

    private SwaggerResponse createSwaggerResponseFromYaml(String yamlContent) throws IOException {
        ObjectMapper mapper = new ObjectMapper(new YAMLFactory());
        mapper.findAndRegisterModules();
        mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
        return mapper.readValue(yamlContent, SwaggerResponse.class);
    }


    /**
     * @param swaggerJsonUrl
     *         full URL to the swagger.json ("https://app.host.domain/ovn/swagger.json")
     * @param swaggerBasePath
     *         the basePath from the swagger.json ("/ovn/rest")
     * @return the URL pointing to the /rest/ endpoints ("https://app.host.domain/ovn/rest") or null
     * if there is a parsing issue
     */
    public String buildBaseUrl(String swaggerJsonUrl, String swaggerBasePath) {
        try {
            URI uri = new URI(swaggerJsonUrl);
            String host = uri.getHost();
            String scheme = uri.getScheme();
            return scheme + "://" + host + swaggerBasePath;
        } catch (URISyntaxException e) {
            return null;
        }
    }
}
