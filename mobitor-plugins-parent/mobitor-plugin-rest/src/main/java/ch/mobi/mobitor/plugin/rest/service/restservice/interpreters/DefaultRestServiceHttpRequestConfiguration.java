package ch.mobi.mobitor.plugin.rest.service.restservice.interpreters;

/*-
 * §
 * mobitor-plugin-rest
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.plugin.rest.RestPluginConfiguration;
import org.apache.http.client.fluent.Request;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.UUID;

import static ch.mobi.mobitor.plugin.rest.service.restservice.RequestHeaderConstants.HEADER_CALLER;
import static ch.mobi.mobitor.plugin.rest.service.restservice.RequestHeaderConstants.HEADER_CALLER_VALUE;
import static ch.mobi.mobitor.plugin.rest.service.restservice.RequestHeaderConstants.HEADER_CALLER_VERSION;
import static ch.mobi.mobitor.plugin.rest.service.restservice.RequestHeaderConstants.HEADER_CORR_ID;

@Component
public class DefaultRestServiceHttpRequestConfiguration {

    private final RestPluginConfiguration restPluginConfiguration;

    @Autowired
    public DefaultRestServiceHttpRequestConfiguration(RestPluginConfiguration restPluginConfiguration) {
        this.restPluginConfiguration = restPluginConfiguration;
    }

    public Request request(String uri) {
        return Request.Get(uri)
                      .addHeader("accept", "application/json")
                      .addHeader("accept", "text/plain") // /liveness in jes7 return plain text
                      .addHeader(HEADER_CALLER, HEADER_CALLER_VALUE)
                      .addHeader(HEADER_CALLER_VERSION, restPluginConfiguration.getVersion())
                      .addHeader(HEADER_CORR_ID, String.valueOf(UUID.randomUUID()))
                      .connectTimeout(2000)
                      .socketTimeout(30000);
    }
}
