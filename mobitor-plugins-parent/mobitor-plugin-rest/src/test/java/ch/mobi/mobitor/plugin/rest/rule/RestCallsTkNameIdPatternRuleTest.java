package ch.mobi.mobitor.plugin.rest.rule;

/*-
 * §
 * mobitor-plugin-rest
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.domain.screen.Pipeline;
import ch.mobi.mobitor.domain.screen.RuleEvaluation;
import ch.mobi.mobitor.plugin.rest.domain.RestCallInformation;
import ch.mobi.mobitor.plugin.test.rule.PipelineRuleTest;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.NullSource;
import org.junit.jupiter.params.provider.ValueSource;

import static ch.mobi.mobitor.plugin.rest.domain.RestCallInformation.REST;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class RestCallsTkNameIdPatternRuleTest extends PipelineRuleTest<RestCallsTkNameIdPatternRule> {

    @Test
    void evaluateRuleWithValidTkNameId() {
        // arrange
        RestCallInformation restInfo = new RestCallInformation();
        restInfo.setSwaggerUri("http://junit.swagger.local");
        restInfo.setTkNameId("aid-fknameid-service");
        Pipeline pipeline = createPipeline();
        pipeline.addInformation(ENV, APP_NAME, restInfo);

        // act
        RuleEvaluation ruleEvaluation = createNewRuleEvaluation();
        evaluateRule(pipeline, ruleEvaluation);

        // assert
        assertThat(ruleEvaluation.hasErrors()).isFalse();
    }

    @Test
    void evaluateRuleWithInvalidTkNameId() {
        // arrange
        RestCallInformation restInfo = new RestCallInformation();
        restInfo.setSwaggerUri("http://junit.swagger.local");
        restInfo.setTkNameId("name");
        Pipeline pipeline = createPipeline();
        pipeline.addInformation(ENV, APP_NAME, restInfo);

        // act
        RuleEvaluation ruleEvaluation = createNewRuleEvaluation();
        evaluateRule(pipeline, ruleEvaluation);

        // assert
        assertThat(ruleEvaluation.hasErrors()).isTrue();
    }

    @Test
    void validatesType() {
        RestCallsTkNameIdPatternRule rule = new RestCallsTkNameIdPatternRule();

        assertThat(rule.validatesType(REST)).isTrue();
    }

    @ParameterizedTest
    @ValueSource(strings = {
            "diu-elan2insign-service",
            "b2e-profil-service",
            "vvn-baustein-service",
            "vvn-baustein-rwc",
            "vvn-baustein-postgresql",
            "abc-def-ghij",
            "abc-sdfsdgasdgfasfd-asdfasghij",
            "abc-d-e",
            "pdv-partnersuche-loader-service",
            "pdv-partnersuche-loader2db-service",
            "pdv-partner4suche-loader4spoud-webapp",
            "peddv-partner4suche-loader4spoud-webapp",
            "peddv-partner4suche-webapp",
            "pddv-partner4suche-webapp",
            "app-name-jeeservice",
            "mps-core-jeeservice",
            "mps-core-b2b-jsfwebclient",
            "mps-core-b2e-jsfwebclient",
            "mps-core-standalone-jsfwebclient",
            "mps-core-siebel-jsfwebclient",
            "mps-core-uid-javabatch",
            "abcdef-cd-service",
            "ab-cd-cd-cd"
    })
    void correct_tkNameId(String tkNameId) {
        assertTrue(isValidTkNameId(tkNameId));
    }

    @ParameterizedTest
    @ValueSource(strings = {
            "ab-cd",
            "a",
            "a_b_c",
            "!/ovn/flotte",
            "",

    })
    @NullSource
    void invalid_tkNameId(String tkNameId) {
        assertFalse(isValidTkNameId(tkNameId));
    }

    private boolean isValidTkNameId(String tkNameId) {
        RestCallsTkNameIdPatternRule tkNameIdRule = new RestCallsTkNameIdPatternRule();
        RestCallInformation restCallInfo = new RestCallInformation();
        restCallInfo.setSwaggerUri("http://swagger.local");
        restCallInfo.setTkNameId(tkNameId);

        return !tkNameIdRule.ruleViolated(restCallInfo);
    }


    @Override
    protected RestCallsTkNameIdPatternRule createNewRule() {
        return new RestCallsTkNameIdPatternRule();
    }
}
