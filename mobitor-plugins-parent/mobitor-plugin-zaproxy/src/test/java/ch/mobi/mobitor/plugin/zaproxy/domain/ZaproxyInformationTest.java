package ch.mobi.mobitor.plugin.zaproxy.domain;

/*-
 * §
 * mobitor-plugin-zaproxy
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class ZaproxyInformationTest {

	private ZaproxyInformation zapInfo;

	@BeforeEach
	public void setUp() {
		this.zapInfo = new ZaproxyInformation("");
		this.zapInfo.setInformationUpdated(true);
	}

	@Test
	public void testGetHighestIssueNumber() {
		// prepare
		zapInfo.setBlockerNumber(2);
		zapInfo.setMajorNumber(5);
		zapInfo.setInfoNumber(12);

		// call reset
		Integer highest = zapInfo.getHighestIssueNumber();

		// check
		assertThat(highest).isEqualTo(2);
	}

	@Test
	public void testGetHighestIssueNumber_0blocker() {
		// prepare
		zapInfo.setMajorNumber(5);
		zapInfo.setInfoNumber(12);

		// call reset
		Integer highest = zapInfo.getHighestIssueNumber();

		// check
		assertThat(highest).isEqualTo(5);
	}

	@Test
	public void testGetHighestIssueNumberMessage_1critical() {
		// prepare
		zapInfo.setCriticalNumber(1);
		zapInfo.setMajorNumber(5);
		zapInfo.setInfoNumber(9);

		// call reset
		String message = zapInfo.getHighestIssueNumberMessage();

		// check
		assertThat(message).contains("1");
	}

	@Test
	public void testGetHighestIssueNumberMessage_3major() {
		// prepare
		zapInfo.setMajorNumber(3);
		zapInfo.setMinorNumber(5);
		zapInfo.setInfoNumber(9);

		// call reset
		String message = zapInfo.getHighestIssueNumberMessage();

		// check
		assertThat(message).contains("3");
	}

	@Test
	public void testGetIssueSummary_WithValues() {
		// prepare
		zapInfo.setBlockerNumber(0);
		zapInfo.setCriticalNumber(1);
		zapInfo.setMajorNumber(3);
		zapInfo.setMinorNumber(5);
		zapInfo.setInfoNumber(9);

		// call reset
		String message = zapInfo.getIssueSummary();

		// check
		assertThat(message).contains("\"Blocker\": 0");
		assertThat(message).contains("\"Critical\": 1");
		assertThat(message).contains("\"Major\": 3");
		assertThat(message).contains("\"Minor\": 5");
		assertThat(message).contains("\"Info\": 9");
	}

	@Test
	public void testGetIssueSummary_NoData() {
		// prepare
		zapInfo.setInformationUpdated(false);

		// call reset
		String message = zapInfo.getIssueSummary();

		// check
		assertThat(message).contains("data not available");
	}

}
