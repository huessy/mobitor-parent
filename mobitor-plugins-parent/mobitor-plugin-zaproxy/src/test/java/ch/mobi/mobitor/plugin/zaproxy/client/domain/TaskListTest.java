package ch.mobi.mobitor.plugin.zaproxy.client.domain;

/*-
 * §
 * mobitor-plugin-zaproxy
 * --
 * Copyright (C) 2019 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import java.util.Collections;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.springframework.test.util.AssertionErrors.assertFalse;
import static org.springframework.test.util.AssertionErrors.assertTrue;

public class TaskListTest {

	private TaskList taskList;

	@BeforeEach
	public void init() {
		TaskInformation ti = new TaskInformation();
		ti.setStatus("SUCCESS");
		taskList = new TaskList();
		taskList.setCurrent(ti);
	}

	@Test
	public void testIsCurrentTaskSuccess() {
		assertTrue("Status should be SUCCESS", taskList.getCurrent().isStatus(TaskInformation.Status.SUCCESS));
	}

	@Test
	public void testIsCurrentTaskNotSuccess() {
		assertFalse("Status should be not FAILED", taskList.getCurrent().isStatus(TaskInformation.Status.FAILED));
	}

	@Test
	public void testIsQueueEmptyWithNull() {
		assertTrue("Null queue should be empty", taskList.isQueueEmpty());
	}

	@Test
	public void testIsQueueEmptyWithEmpty() {
		taskList.setQueue(Collections.EMPTY_LIST);
		assertTrue("Empty queue should be empty", taskList.isQueueEmpty());
	}

}
