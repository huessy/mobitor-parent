= Mobitor EDWH Plugin

link:../../index.html[Back Mobitor Overview]

== Features

This plugin retrieves deployment information of installed ETL packages from a service.

The service the plugin queries is configured in `classpath:mobitor/plugin/edwh/config/dwh-etl-servers.json`


== Configuration

.screen.json
[source,json]
----
"edwhDeployments": [
  {
    "serverName": "DMSCHAD",
    "applicationNames": [
      "DATASTAGE",
      "DATABASE",
      "QUERYSURGE"
    ]
  }
----

.Parameters
|===
| Name | Description | Required | Default

| serverName
| The server name that refers to the serverNames within the same screen, used to reference the position of this information block in the screen
| yes
| empty

| applicationNames
| The application names to define and refer to the sub-row of the server name in the same screen. Used to reference the position if this information block in the screen
| yes
| empty
|===

The plugin will use the `environments` from the same screen to query the service.


== Result

The plugin fills information blocks that provide version information on a screen:

image::edwh-information-block.png[]
